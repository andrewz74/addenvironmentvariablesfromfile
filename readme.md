# AddEnvironmentVariablesFromFile

This package contains new configuration provider
to get values from file.

This provider is similar to `AddEnvironmentVariables`:

```

static void Main(string[] args)
{
	var cfg = InitOptions<AppConfig>();
	string server = cfg.Server;
	string userId = cfg.UserName;
	...
}

private static T InitOptions<T>() where T : new()
{
	var config = InitConfig();
	return config.Get<T>();
}

private static IConfigurationRoot InitConfig()
{
	var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
	var builder = new ConfigurationBuilder()
		.AddJsonFile($"appsettings.json", true, true)
		.AddJsonFile($"appsettings.{env}.json", true, true)
		.AddEnvironmentVariables();

	return builder.Build();
}

```

In this case if exist token in json file that exist in AppConfig
class like a property, is populated with this value.

Adding new provider:

```
var builder = new ConfigurationBuilder()
	.AddJsonFile($"appsettings.json", true, true)
	.AddJsonFile($"appsettings.{env}.json", true, true)
	.AddEnvironmentVariables()
	.AddEnvironmentVariablesFromFile();
```

Provider searches environment variables ending with suffix `_FILE`
(there is also a parameter in `AddEnvironmentVariablesFromFile`
method to change it) and the content will be used as the file name.
Then the contents of this file are read and entered as a value.

In previous example, `AppConfig` class contains property `Server`
and if Environoment Variable contains:
- Server_FILE = /tmp/server.txt

Provider read the content of this file, and insert value into
`Server` property.

This is useful in Docker containers or Kubernetes for passing parameters like secrets or config.



## License

MIT ©
