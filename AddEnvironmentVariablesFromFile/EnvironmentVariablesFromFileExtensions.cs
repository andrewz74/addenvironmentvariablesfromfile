﻿using Microsoft.Extensions.Configuration.EnvironmentVariables;

namespace Microsoft.Extensions.Configuration
{
	public static class EnvironmentVariablesFromFileExtensions
	{
		public static IConfigurationBuilder AddEnvironmentVariablesFromFile(this IConfigurationBuilder configurationBuilder)
		{
			configurationBuilder.Add(new EnvironmentVariablesFromFileConfigurationSource());
			return configurationBuilder;
		}
		public static IConfigurationBuilder AddEnvironmentVariablesFromFile(this IConfigurationBuilder configurationBuilder, string prefix)
		{
			configurationBuilder.Add(new EnvironmentVariablesFromFileConfigurationSource
			{
				Prefix = prefix
			});

			return configurationBuilder;
		}
		public static IConfigurationBuilder AddEnvironmentVariablesFromFile(this IConfigurationBuilder configurationBuilder, string prefix, string suffix)
		{
			configurationBuilder.Add(new EnvironmentVariablesFromFileConfigurationSource
			{
				Prefix = prefix,
				Suffix = suffix
			});

			return configurationBuilder;
		}
	}
}
