﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Microsoft.Extensions.Configuration.EnvironmentVariables
{
	public class EnvironmentVariablesFromFileConfigurationProvider : ConfigurationProvider
	{
		private readonly string _prefix;
		private readonly string _suffix;

		public EnvironmentVariablesFromFileConfigurationProvider()
			: this(null)
		{
		}

		public EnvironmentVariablesFromFileConfigurationProvider(string prefix)
			: this(prefix, null)
		{
		}

		public EnvironmentVariablesFromFileConfigurationProvider(string prefix, string suffix)
		{
			_prefix = prefix ?? string.Empty;
			_suffix = suffix ?? "_FILE";
		}

		public override void Load()
		{
			Load(Environment.GetEnvironmentVariables());
		}

		internal void Load(IDictionary envVariables)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
			IEnumerable<DictionaryEntry> enumerable = envVariables.Cast<DictionaryEntry>()
				.Where(t => t.Key.ToString().StartsWith(_prefix, StringComparison.OrdinalIgnoreCase))
				.Where(t => t.Key.ToString().EndsWith(_suffix))
				.Select(t => new DictionaryEntry
				{
					Key = t.Key.ToString().Substring(0, t.Key.ToString().Length - _suffix.Length),
					Value = t.Value
				});

			foreach (DictionaryEntry item in enumerable)
			{
				string key = item.Key.ToString().Substring(_prefix.Length);
				string contentFile = FileContent(item.Value.ToString());
				if (!string.IsNullOrEmpty(contentFile))
				{
					dictionary[key] = contentFile;
				}
			}

			base.Data = dictionary;
		}

		private string FileContent(string filename)
		{
			if (!File.Exists(filename))
			{
				return null;
			}

			try
			{
				return File.ReadAllText(filename).Trim();
			}
			catch
			{
				return null;
			}
		}
	}
}
