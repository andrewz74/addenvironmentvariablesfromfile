﻿namespace Microsoft.Extensions.Configuration.EnvironmentVariables
{
	public class EnvironmentVariablesFromFileConfigurationSource : IConfigurationSource
	{
		public string Prefix { get; set; }
		public string Suffix { get; set; }

		public IConfigurationProvider Build(IConfigurationBuilder builder)
		{
			return new EnvironmentVariablesFromFileConfigurationProvider(Prefix, Suffix);
		}
	}
}
