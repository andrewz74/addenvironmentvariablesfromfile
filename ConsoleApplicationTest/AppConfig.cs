﻿namespace ConsoleApplicationTest
{
    public class AppConfig
    {
        public string Server { get; set; }
        public string DatabaseName { get; set; }
        public Credentials Credentials { get; set; }
    }

    public class Credentials
    {
        public string UserId { get; set; }
        public string Password { get; set; }
    }
}
