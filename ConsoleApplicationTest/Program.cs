﻿using Microsoft.Extensions.Configuration;
using System;
using System.Xml.Serialization;

namespace ConsoleApplicationTest
{
	class Program
	{
		static void Main(string[] args)
		{
			var cfg = InitOptions<AppConfig>();
			var xml = new XmlSerializer(cfg.GetType());
			xml.Serialize(Console.Out, cfg);
		}

		private static T InitOptions<T>() where T : new()
		{
			var config = InitConfig();
			return config.Get<T>();
		}

		private static IConfigurationRoot InitConfig()
		{
			var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
			var builder = new ConfigurationBuilder()
				.AddJsonFile($"appsettings.json", true, true)
				.AddJsonFile($"appsettings.{env}.json", true, true)
				.AddEnvironmentVariables()
				.AddEnvironmentVariablesFromFile(); // or (null, "_FILE") or ("PREFIX_", "_FILE")

			return builder.Build();
		}
	}
}
